package main

import (
	"log"

	"gitlab.com/bazzz/logging"
	"gitlab.com/bazzz/reception"
)

func main() {
	logging.Connect()
	log.SetOutput(logging.Default())
	log.Fatal("ERROR", reception.Start())
}
