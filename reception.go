package reception

import (
	"errors"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"

	"gitlab.com/bazzz/cfg"
)

var targets = make(map[string]*url.URL)

// Start initializes the reverse proxy.
func Start() error {
	log.Print("Start")
	config, err := cfg.New()
	if err != nil {
		return err
	}
	targetlines := config.Get("targets")
	if targetlines == "" {
		return errors.New("targets is empty")
	}
	if err := readTargets(targetlines); err != nil {
		return errors.New("could not parse target urls: " + err.Error())
	}
	host := config.Get("host")
	port := config.Get("port")
	cert := config.Get("certfile")
	key := config.Get("keyfile")
	http.HandleFunc("/", handle)
	return http.ListenAndServeTLS(host+":"+port, cert, key, nil)
}

func handle(w http.ResponseWriter, r *http.Request) {
	host := r.Host
	log.Print("Handling host:", host)
	if target, ok := targets[host]; ok {
		proxy := httputil.NewSingleHostReverseProxy(target)
		proxy.ServeHTTP(w, r)
	}
}

func readTargets(input string) error {
	targetlines := strings.Split(input, ",")
	for _, targetline := range targetlines {
		parts := strings.Split(targetline, ">")
		if len(parts) != 2 {
			continue
		}
		host := strings.Trim(parts[0], " \t")
		targetString := strings.Trim(parts[1], " \t")
		target, err := url.Parse(targetString)
		if err != nil {
			return err
		}
		targets[host] = target
	}
	return nil
}
