# reception

Simple reverse proxy to reroute (sub)domains to different urls.

A common usage would be:

    func main() {
        log.Fatal(reception.Start())
    }

Note that reception expects there to be a reception.conf file in the same directory as the executable. An example conf file is provided.