module gitlab.com/bazzz/reception

go 1.21

require (
	gitlab.com/bazzz/cfg v0.0.0-20211008083346-0087e658624c
	gitlab.com/bazzz/logging v0.0.0-20230919074736-179b967e340a
)
